def main():
    port_num = str(input("Please enter your new ssh port "))
    sshd_config = open('my_sshd_config', 'rt')
    write_file = open('sshd_config', 'wt')
    for line in sshd_config:
        write_file.write(line.replace('#Port 22', "Port " + port_num).replace('#LoginGraceTime 2m', "LoginGraceTime 1m").replace('#MaxAuthTries 6', "MaxAuthTries 1").replace('#MaxSessions 10', "MaxSessions 1"))
    sshd_config.close()
    write_file.close()
main()
