#!/bin/bash
echo "######################################################################"
echo "Hello 007"
echo "Getting your Server Ready"
echo "######################################################################"
sudo apt-get update
echo "Update Done ##########################################################"
sudo apt-get upgrade 
echo "Upgrade Done #########################################################"
sudo apt-get install nginx
echo "Nginx Installed ######################################################"
sudo ufw enable
echo "UFW Enabled ##########################################################"
sudo ufw allow  'Nginx HTTP'
echo "UFW HTTP Allowed #####################################################"
sudo ufw allow  'Nginx HTTPS'
echo "UFW HTTPS Allowed #####################################################"
sudo ufw allow  'Nginx FULL'
echo "UFW Nginx Full Allowed ################################################"
sudo ufw allow 'OpenSSH'
echo "UFW OpenSSH Allowed ##################################################"
sudo systemctl status nginx
echo "######################################################################"
echo "NGINX SystemCTL Passed ###############################################"
nginx -t 
echo "Check to see if Nginx Test Passed ####################################"
echo "If everything looks good - sudo systemctl restart nginx ##############"
echo "######################################################################"
echo "Hello 007"
echo "Getting Node & NPM Ready"
echo "######################################################################"
sudo apt-get install npm 
npm install -g npm@latest
echo "NPM Done #############################################################"
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
echo "NodeJS Done ##########################################################"
npm install pm2 -g
echo "PM2 Done #############################################################"
echo "#######################################################################"
echo "Hello 007"
echo "Q is Configuring Nginx for You"
echo "#######################################################################"
echo -n "Enter your URL (ex. 'your_url.com' no 'https://') \"url\" "
read user_url
python3 backend_server_block_config.py
cp $user_url ~/../etc/nginx/sites-available/
sudo ln -s ~/../etc/nginx/sites-available/$user_url ~/../etc/nginx/sites-enabled/
nginx -t
sudo systemctl restart nginx
cd ~/
git clone https://gitlab.com/B3PO/node-express-mongo-template